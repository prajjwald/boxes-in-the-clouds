# Discussion Topics

## Potential Locations

- school
- rural, no internet access
- need point of contact(??)

## Potential Devices

### raspberry pi

#### Pros

- widely used
- easy to setup
- relatively cheap

#### Cons

- potential for theft

## Old Android Phones

#### Pros

- somewhat cheap
- small form factor

#### Cons

- harder to setup
- hard to maintain due to non-uniform setup
- battery?